import 'dart:convert';
import 'dart:ffi';
import 'dart:io';
import 'dart:math';
import 'Calculator.dart';

class calculator {
  double num1 = 0.0;
  var operator;
  double num2 = 0.0;

  //contructor
  calculator(double firstNumber, double secondNumber) {
    this.num1 = firstNumber;
    this.num2 = secondNumber;
  }

  double add(double firstNum, double secondNum) {
    return firstNum + secondNum;
  }

  double subtract(double firstNum, double secondNum) {
    return firstNum - secondNum;
  }

  double multiply(double firstNum, double secondNum) {
    return firstNum * secondNum;
  }

  num exponent(double firstNum, double secondNum) {
    num expon = pow(firstNum, secondNum);
    return expon;
  }

  double divide(double firstNum, double secondNum) {
    return firstNum / secondNum;
  }

  static void result() {
    double firstNum = 0.0;
    double secondNum = 0.0;

    String InputNum1 = stdin.readLineSync()!;
    //cut spac
    InputNum1.trim();

    String Operator = stdin.readLineSync()!;
    //cut spac
    Operator.trim();

    String InputNum2 = stdin.readLineSync()!;
    //cut spac
    InputNum2.trim();

    //change String  to double
    //InputNum1
    if (InputNum1 == 'exit') {
      exit(0); //close
    } else {
      firstNum = double.parse(InputNum1);
    }

    ////InputNum2
    if (InputNum2 == 'exit') {
      exit(0); //close
    } else {
      secondNum = double.parse(InputNum2);
    }

    calculator cal1 = calculator(firstNum, secondNum);

    switch (Operator) {
      case "+":
        print(
            " ${cal1.num1} + ${cal1.num2} = ${cal1.add(firstNum, secondNum)}");
        break;
      case "-":
        print(
            " ${cal1.num1} - ${cal1.num2} = ${cal1.subtract(firstNum, secondNum)}");
        break;
      case "*":
        print(
            " ${cal1.num1} * ${cal1.num2} = ${cal1.multiply(firstNum, secondNum)}");
        break;
      case "/":
        if (secondNum != 0) {
          print("error");
        }
        break;
      case "^":
        print(
            " ${cal1.num1} / ${cal1.num2} = ${cal1.exponent(firstNum, secondNum)}");
        break;
    }
  }
}

void main(List<String> args) {
  calculator.result();
}
